# water-module

Javascript for the ESP32 / ESP8266 - I2C Atlas Scientific PH & EC Sensors, running with [Espruino](http://www.espruino.com/). Note that you must already have Espruino >= v.1.98.

Instructions for [flashing the ESP32 can be found here.](http://www.espruino.com/ESP32), and for [the ESP8266, here](http://www.espruino.com/EspruinoESP8266).

### Connectivity for the node-red-contrib-deviceManager modules

This module provides hardware access to pH, EC & Temperature values via the
[Atlas pH](https://www.atlas-scientific.com/product_pages/kits/ph-kit.html), [Atlas EC](https://www.atlas-scientific.com/product_pages/kits/ec_k1_0_kit.html) & the waterproof DS18B20 temperature sensor.

This firmware should be loaded onto an ESP32 (or, an ESP8266), with the Atlas EC, pH & DS18B20 connected, and a node-red instance running with the 
[node-red-contrib-deviceManager](https://gitlab.com/jgtaylor/node-red-contrib-deviceManager) installed & configured.

The pH & EC Sensors should be configured for I2C communication. Instructions for configuring I2C can be found in the [EC_EZO datasheet](https://www.atlas-scientific.com/_files/_datasheets/_circuit/EC_EZO_Datasheet.pdf) on Pg. 41, & in the [pH_EZO datasheet](https://www.atlas-scientific.com/_files/_datasheets/_circuit/pH_EZO_Datasheet.pdf) on Pg. 39. One can also find the wiring requirements there.

The DS18B20 is widely available and requires no configuation.

### Configuration

It should only be necessary to change the values for `server`, `SSID`, and `ssidPassword`.

One can set them for their own environment. Server means the node-red server IP address.