/* 
  this module is intended for Espruino, available at Espruino.com, & for the Atlas-Scientific EZO circuit.
  see the docs at  https://www.atlas-scientific.com/_files/_datasheets/_circuit/EC_EZO_Datasheet.pdf
  commands to support: Cal, Factory, Find, i?, I2C?, K, L, O?, R, Sleep?, Status?, T
  for Calibration, see page 10 of the above document. 
*/
"use strict"


/**
 * @param {number} address
 * @param {Object} i2cInstance
 * @returns {Object}
 */
module.exports = (address, i2cInstance) => {
  function ua2text(ua) {
    var s = '';
    for (var i = 1; i < ua.length; i++) {
      if (ua[i] !== 0x01 && ua[i] !== 0x00) {
        s += String.fromCharCode(ua[i]);
      }
    }
    return s;
  }
  /**
   * 
   * @param {number} d - the result of the last i2c operation
   */
  function retStatus(d) {
    var status = "";
    switch (d) {
      case 0x01:
        {
          return status = "success";
        }
      case 0x02:
        {
          return status = "syntax error";
        }
      case 0xfe:
        {
          return status = "still processing, not ready";
        }
      case 0xff:
        {
          return status = "no data to send";
        }
    }
  }
  var atlasEC = {};

  /**
   * @param {string} type - one of ["dry", "low", "high", "?"]
   * @param {function} cb - call back for return values or output for "?"
   */
  atlasEC.calibrate = (type, cb) => {
    // assumes use of Atlas-Scientific's solutions
    let _cmdString = "";
    let _delay = 600; // ms to wait before reading output
    switch (type) {
      case "dry":
        {
          _cmdString = "Cal,dry";
          break;
        }
      case "low":
        {
          _cmdString = "Cal,low,12800";
          break;
        }
      case "high":
        {
          _cmdString = "Cal,high,80000";
          break;
        }
      case "?":
        {
          _cmdString = "Cal,?";
          _delay = 300;
          break;
        }
    }
    i2cInstance.writeTo(address, _cmdString);
    if (cb) {
      setTimeout(function () {
        let rawData = i2cInstance.readFrom(address, 32);
        if (retStatus(rawData[0]) != "success") {
          cb(retStatus(rawData[0]));
          return
        }
        if (_cmdString == "Cal,?") {
          cb(ua2text(rawData).split(","));
        }
      }, _delay);
    }

  }
  atlasEC.find = () => {
    let _cmdString = "Find";
    i2cInstance.writeTo(address, _cmdString);
  }
  atlasEC.factory = () => {
    let _cmdString = "Factory";
    i2cInstance.writeTo(address, _cmdString);
  }
  /**
   * @param {number} pType - any floating point number; describes the probe type K 1.0 is default 
   */
  atlasEC.probeType = (pType) => {
    let _cmdString = "K," + pType;
    i2cInstance.writeTo(address, _cmdString);
  }
  /**
   * @param ({boolean}|{number}) - 0 = off, 1 = on, 
   */
  atlasEC.LED = (state) => {
    if (state == true) {
      state = 1;
    } else {
      state = 0;
    }
    let _cmdString = "L," + state;
    i2cInstance.writeTo(address, _cmdString);
  }
  /**
   * @param {function} cb - recieves the result of the read operation
   */
  atlasEC.read = (cb) => {
    /* send the command, wait 600ms, get the data and give it to the callback. */
    i2cInstance.writeTo(address, "R");
    setTimeout(function () {
      let rawData = i2cInstance.readFrom(address, 31);
      if (retStatus(rawData[0]) != "success") {
        cb(retStatus(rawData[0]));
        return
      }
      let _s = ua2text(rawData).split(",");
      _s.forEach((el, i) => {
        switch (i) {
          case 0:
            {
              EC = parseInt(el);
              break;
            }
          case 1:
            {
              tds = parseInt(el);
              break;
            }
          case 2:
            {
              salinity = parseFloat(el);
              break;
            }
          case 3:
            {
              specGrav = parseFloat(el);
              break;
            }
        }
      })
      cb({
        EC: EC,
        tds: tds,
        salinity: salinity,
        specGrav: specGrav
      });
    }, 600);
  }
  /**
   * @param {function} cb - recieves the result of the read operation
   * @param {number} temp - is the current temperature in celsius.
   */
  atlasEC.readTempCompensated = (temp, cb) => {
    /* send the command, wait 600ms, get the data and give it to the callback. */
    let _cmdString = "RT," + temp;
    i2cInstance.writeTo(address, _cmdString);
    setTimeout(function () {
      let rawData = i2cInstance.readFrom(address, 31);
      if (retStatus(rawData[0]) != "success") {
        cb(retStatus(rawData[0]));
        return
      }
      let _s = ua2text(rawData).split(",");
      _s.forEach((el, i) => {
        switch (i) {
          case 0:
            {
              EC = parseInt(el);
              break;
            }
          case 1:
            {
              tds = parseInt(el);
              break;
            }
          case 2:
            {
              salinity = parseFloat(el);
              break;
            }
          case 3:
            {
              specGrav = parseFloat(el);
              break;
            }
        }
      })
      cb({
        EC: EC,
        tds: tds,
        salinity: salinity,
        specGrav: specGrav
      });
    }, 600);
  }
  return atlasEC;
}