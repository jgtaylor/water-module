var atlasEC = require( "https://gitlab.com/jgtaylor/atlas-ec-i2c-for-espruino/raw/master/atlas-ec.js" );
var atlasPH = require( "https://gitlab.com/jgtaylor/atlas-ph-i2c-for-espruino/raw/master/atlas-ph.js" );
var http = require( "http" );
var server = "192.168.2.122",	SSID = "X11",	ssidPassword = "secret99", wsPath = "/devices/water", wsPort = 1880;
var DebugOptions = {host: server, port: wsPort, path: "/debug", method: "POST", protocol: "http:"};

I2C1.setup( {
  scl: D17,
  sda: D16
} );
var ec = atlasEC( 100, I2C1 );
var ph = atlasPH( 99, I2C1 );
var w = require( "Wifi" );
var ow = new OneWire( D14 );
var tempSensor = require( "DS18B20" ).connect( ow );
var WebSocket = require( "ws" );
var WebSock = {};
var configMap = [
  {
    id: "AtlasEC",
    pin: function ( cmd ) {
      if ( typeof cmd == "string" ) {
        switch ( cmd ) {
          case "read":
          {
            let that = this;
            ec.read( function ( m ) {
              WebSock.send( JSON.stringify( ["reading", {
                device: that.id,
                value: m
              }] ) );
            } );
            break;
          }
          case "readTempCompensated": 
          {
            if ( tempSensor ) {
              var that = this;
              tempSensor.getTemp( function( _t ) {
                ec.readTempCompensated( _t, function ( m ) {
                  WebSock.send( JSON.stringify( ["reading",{device: that.id,value: m}] ) );
                } );
              } );
            }
            break;
          }
          default:
            break;
        }
      }
    },
    type: "virtual",
    validCmds: ["read", "readTempCompensated"],
    meta: {
      keys: [{
        name: "EC",
        metric: "EC",
        unit: "mS"
      }],
      deviceName: "AtlasEC"
    }
  },
  {
    id: "AtlasPH",
    pin: function ( cmd ) {
      switch ( cmd ) {
        case "read":
        {
          let that = this;
          ph.read( function ( m ) {
            WebSock.send( JSON.stringify( ["reading", {device: that.id,value: m}] ) );
          } );
          break;
        }
        case "readTempCompensated": 
        {
          if ( tempSensor ) {
            var that = this;
            tempSensor.getTemp( function( _t ) {
              ph.readTempCompensated( _t, function ( m ) {
                WebSock.send( JSON.stringify( ["reading",{device: that.id,value: m}] ) );
              } );
            } );
          }
          break;
        }
        default:
          break;
      }
    },
    type: "virtual",
    validCmds: ["read", "readTempCompensated"],
    meta: {
      keys: [{
        name: "PH",
        metric: "PH",
        unit: "PH"
      }],
      deviceName: "AtlasPH"
    }
  }, {
    id: "DS18B20",
    pin: function ( cmd ) {
      var that = this;
      switch ( cmd ) {
        case "read":
        {
          tempSensor.getTemp( ( temp ) => {
            WebSock.send( JSON.stringify( ["reading", {device: that.id,value: { Temperature: temp }}] ) );
          } );
          break;
        }
        default:
          break;
      }
    },
    type: "virtual",
    validCmds: ["read"],
    meta: {
      keys: [{
        name: "Temperature",
        metric: "Temperature",
        unit: "C"
      }],
      deviceName: "DS18B20"
    }
  }
];

function configGen( config ) {
  let ret = [];
  config.forEach( ( el ) => {
    let t = {
      device: el.id,
      type: el.type,
      validCmds: el.validCmds,
      meta: el.meta
    };
    ret.push( t );
  } );
  return ret;
}

function button( d, cmd ) {
  if ( d.pin.getMode() !== "output" ) {
    d.pin.mode( "output" );
  }
  switch ( cmd ) {
    case "on":
    {
      digitalWrite( d.pin, 1 );
      let retMsg = ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }];
      WebSock.send( JSON.stringify( retMsg ) );
      break;
    }
    case "off":
    {
      digitalWrite( d.pin, 0 );
      let retMsg = ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }];
      WebSock.send( JSON.stringify( retMsg ) );
      break;
    }
    case "getState":
    {
      WebSock.send( JSON.stringify( ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }] ) );
      break;
    }
    default:
    {
      break;
    }
  }
}

function dimmer( d, cmd ) {
  switch ( cmd ) {
    case "read":
      {
        WebSock.send( JSON.stringify( ["reading", {
          device: d.id,
          value: analogRead()
        }] ) );
      }
      break;
    default:
      analogRead();
      break;

  }
}

function virtual( d, cmd ) {
  d.pin( cmd );
}

function msgParse( msg ) {
  var m = JSON.parse( msg );

  function device( map ) {
    for ( let x = 0; x < map.length; x++ ) {
      if ( map[x].id === m[1].device ) {
        return map[x];
      }
    }
    return {
      id: null,
      pin: null,
      type: null,
      validCmds: null,
      meta: {
        keys: [{
          name: null,
          metric: null,
          unit: null
        }],
        deviceName: null
      }
    };
  }
  switch ( m[0] ) {
    case "cmd":
    {
      let d = device( configMap );
      switch ( d.type ) {
        case "button":
        {
          button( d, m[1].cmd );
          break;
        }
        case "virtual":
        {
          virtual( d, m[1].cmd );
          break;
        }
        case "dimmer":
        {
          dimmer( d, m[1].cmd );
          break;
        }
        default:
          break;
      }
      break;
    }
    case "config":
    {
      WebSock.send( JSON.stringify( ["config", configGen( configMap )] ) );
      break;
    }
    default:
      break;
  }
}

function WebSockconnect( state ) {
  if ( state === 1 ) {
    WebSock.removeAllListeners();
    WebSock = null;
  }
  WebSock = new WebSocket( server, {
    path: wsPath,
    port: wsPort,
    origin: "MCU",
    keepAlive: 60
  } );
  WebSock.on( "error", ( err ) => {
    let data = `ERROR: Websocket Error\nError is: ${err}\n${process.memory()}`;
    http.request( DebugOptions, ( res ) => { console.log( res );} ).end( data );
  } );
  WebSock.on( "open", () => {
    WebSock.send( JSON.stringify( ["config", configGen( configMap )] ) );
  } );
  WebSock.on( "close", ( code, reason ) => {
    setTimeout( function () {
      WebSockconnect( 1 );
    }, 10000 ); // 10 seconds between reconnect attempts.
    let data = `ERROR: Websocket disconnected\nReason: ${reason}\n${process.memory()}`;
    http.request( DebugOptions, ( res ) => { console.log( res );} ).end( data );
  } );

  WebSock.on( "message", ( msg ) => {
    msgParse( msg.toString() );
  } );

  WebSock.on( "ping", function() { WebSock.pong( "a" );} );

}

E.on( "init", () => {
  w.stopAP();
  w.on( "disconnected", () => {
    w.connect( SSID, {
      password: ssidPassword
    }, function ( error ) {
      console.log( error );
      w.startAP();
    } );
  } );
  w.on( "connected", () => { WebSockconnect( 0 ); } );
  w.connect( SSID, {
    password: ssidPassword
  }, ( error ) => {
    if ( error ) {
      console.log( error );
      w.startAP();
    }
  } );
} );