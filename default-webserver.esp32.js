var wifi = require( "Wifi" ), fs = require( "fs" ), http = require( "http" ), storage = require( "Storage" );

function dataStoreFS( req, res ) {
  var totalExpected = parseInt( req.headers["Content-Length"] ); // fixed!
  console.log(`Total bytes should be: ${totalExpected}`);
  var q = url.parse( req.url, true );
  var name = q.pathname;
  var f = E.openFile( name, "w+" );
  var buf = "", finalSize = 0;
  req.on( "data", function ( data ) {
    //console.log(`Getting data...\n${data}`);
    finalSize = finalSize + data.length;
    buf += data;
    let _dirty = true;
    if ( buf.length > 4096 ) { 
      f.write( buf );
      //console.log(`wrote ${buf.length}`);
      buf = "";
      _dirty = false;
    }
    if ( finalSize == totalExpected ) {
      if ( _dirty ) {
        f.write( buf );
        f.close();
      }
      res.writeHead( 201, {"Content-type": "text/plain"} );
      res.write( `Total expected: ${totalExpected}\nTotal written: ${finalSize}` );
      res.end();
    }

  } );
}

function setupSystem( config ) {
  if ( storage.list().filter( ( e )=>{return e == "ptCfg";} ) ) {
    var _config = storage.erase( "ptCfg" );
  }
  if ( config.ssid !== "" ) { 
    if ( config.ssid.length <= 32 ) { // max 32byte name length
      console.log( "wifi.connect(config.ssid, {password: config.password})" );
    }
  }
//  {
//    "ssid": "",
//    "wifiPassword": "",
//    "serverAddress": "",
//    "serverPort": "",
//    "serverPath": ""
//   }
}

function reqGet( req, res ) {
  var q = url.parse( req.url, true );
  var type;
  var name = q.pathname.replace( "/","" );
  var ext;
  if ( name === "" ) {
    name = "index.html";
  }
  ext = name.split( "." ).pop();
  
  switch ( ext ) {
    case "html":
      type = {"Content-Type": "text/html"};
      break;
    case "css":
      type = {"Content-Type": "text/css"};
      break;
    case "svg":
      type = {"Content-Type": "image/svg+xml"};
      break;
    case "ico":
      type = {"Content-Type": "image/vnd.microsoft.icon"};
      break;
    default:
      type = {"Content-Type": "text/html"};
      break;
  }
  res.writeHead( 200, type );
  //console.log( "file name: ", name );
  try {
    E.openFile( name, "r" ).pipe( res, {chunksize: 4096, end: true} );
  } catch ( e ) {
    console.log( e );
  }
  if ( q.query ) {
    setupSystem( q.query );
  }
}

function onPageRequest( req, res ) {
  if ( req.method == "GET" ) {
    reqGet( req, res );
  }
  if ( req.method == "PUT" ) {
    //console.log(req);
    dataStoreFS( req, res );
  }
}

/*
wifi.on("connected", () => {
  console.log("wifi connected");
  http.createServer(onPageRequest).listen(80);
});
wifi.on("disconnected", (detail) => {
  console.log(detail);
});

if (wifi.getStatus().station == "connected") { 
  h = http.createServer(onPageRequest).listen(80); 
} else {
  wifi.connect("X11", {password: "secret99"});
}
*/
if (wifi.getDetails().status != "connected") {
  wifi.startAP("breal", null, function(d) {
    http.createServer(onPageRequest).listen(80);
  });
}
