module.exports = function ( address, i2cInstance ) {
  function ua2text( ua ) {
    var s = "";
    for ( var i = 1; i < ua.length; i++ ) {
      if ( ua[i] !== 0x01 && ua[i] !== 0x00 ) {
        s += String.fromCharCode( ua[i] );
      }
    }
    return s;
  }
  function retStatus( d ) {
    var status = "";
    switch ( d ) {
      case 0x01:
      {
        status = "success";
        return status;
      }
      case 0x02:
      {
        status = "syntax error";
        return status;
      }
      case 0xfe:
      {
        status = "still processing, not ready";
        return status;
      }
      case 0xff:
      {
        status = "no data to send";
        return status;
      }
    }
  }
  var atlasEC = {};
  atlasEC.calibrate = ( type, cb ) => {
    var _cmdString = "";
    var _delay = 600;
    switch ( type ) {
      case "dry":
      {
        _cmdString = "Cal,dry";
        break;
      }
      case "low":
      {
        _cmdString = "Cal,low,12800";
        break;
      }
      case "high":
      {
        _cmdString = "Cal,high,80000";
        break;
      }
      case "?":
      {
        _cmdString = "Cal,?";
        _delay = 300;
        break;
      }
      default: break;
    }
    i2cInstance.writeTo( address, _cmdString );
    if ( cb ) {
      setTimeout( function () {
        var rawData = i2cInstance.readFrom( address, 32 );
        if ( retStatus( rawData[0] ) != "success" ) {
          cb( retStatus( rawData[0] ) );
          return;
        }
        if ( _cmdString == "Cal,?" ) {
          cb( ua2text( rawData ).split( "," ) );
        }
      }, _delay );
    }

  };
  atlasEC.find = () => {
    var _cmdString = "Find";
    i2cInstance.writeTo( address, _cmdString );
  };
  atlasEC.factory = () => {
    let _cmdString = "Factory";
    i2cInstance.writeTo( address, _cmdString );
  };
  atlasEC.probeType = ( pType ) => {
    let _cmdString = "K," + pType;
    i2cInstance.writeTo( address, _cmdString );
  };
  atlasEC.LED = ( state ) => {
    if ( state == true ) {
      state = 1;
    } else {
      state = 0;
    }
    let _cmdString = "L," + state;
    i2cInstance.writeTo( address, _cmdString );
  };
  atlasEC.read = ( cb ) => {
    i2cInstance.writeTo( address, "R" );
    setTimeout( function () {
      let rawData = i2cInstance.readFrom( address, 31 );
      if ( retStatus( rawData[0] ) != "success" ) {
        cb( retStatus( rawData[0] ) );
        return;
      }
      var EC, tds, salinity, specGrav;
      let _s = ua2text( rawData ).split( "," );
      _s.forEach( ( el, i ) => {
        switch ( i ) {
          case 0:
          {
            EC = parseInt( el );
            break;
          }
          case 1:
          {
            tds = parseInt( el );
            break;
          }
          case 2:
          {
            salinity = parseFloat( el );
            break;
          }
          case 3:
          {
            specGrav = parseFloat( el );
            break;
          }
        }
      } );
      cb( {
        EC: EC,
        tds: tds|null,
        salinity: salinity|null,
        specGrav: specGrav|null
      } );
    }, 600 );
  };
  atlasEC.readTempCompensated = ( temp, cb ) => {
    let _cmdString = "RT," + temp;
    let that = this;
    i2cInstance.writeTo( address, _cmdString );
    setTimeout( function () {
      let rawData = i2cInstance.readFrom( address, 31 );
      if ( retStatus( rawData[0] ) != "success" ) {
        cb( retStatus( rawData[0] ) );
        return;
      }
      var EC, tds, salinity, specGrav;
      let _s = ua2text( rawData ).split( "," );
      _s.forEach( ( el, i ) => {
        switch ( i ) {
          case 0:
          {
            EC = parseInt( el );
            break;
          }
          case 1:
          {
            tds = parseInt( el );
            break;
          }
          case 2:
          {
            salinity = parseFloat( el );
            break;
          }
          case 3:
          {
            specGrav = parseFloat( el );
            break;
          }
        }
      } );
      cb( {
        EC: EC,
        tds: tds|null,
        salinity: salinity|null,
        specGrav: specGrav|null
      } );
    }, 600 );
  };
  return atlasEC;
};